﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Simbolo
    {
        public string Token { get; set; }
        public string Lexema { get; set; }
    }
}
