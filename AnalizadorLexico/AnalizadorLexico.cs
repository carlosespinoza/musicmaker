﻿using COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizadorLexico
{
    public class AnalizadorLexico
    {
        public List<Simbolo> TablaDeSimbolos { get; private set; }
        string codigo;
        public AnalizadorLexico(string codigoOriginal)
        {
            TablaDeSimbolos = new List<Simbolo>();
            char[] saltoDeLinea = { '\n', '\r' };
            //separar por lineas
            string[] lineas = codigoOriginal.Split(saltoDeLinea);
            foreach (var linea in lineas.ToList())
            {
                AnalizarLinea(linea, 0, 0);
            }
        }

        private void AnalizarLinea(string linea,int pos,int estado)
        {
            switch (estado)
            {
                case 0:
                    if (linea[pos] == 'r')
                        AnalizarLinea(linea, pos + 1, 1);
                    if (linea[pos] == '<')
                        AnalizarLinea(linea, pos + 1, 12);
                    if (linea[pos] == '@')
                        AnalizarLinea(linea, pos + 1, 16);
                    GeneraToken(linea, "Error");
                break;
                case 1:
                    if (linea[pos] == 'e')
                        AnalizarLinea(linea, pos + 1, 2);
                    GeneraToken(linea, "Error");
                break;
                case 2:
                    if (linea[pos] == 'p')
                        AnalizarLinea(linea, pos + 1, 3);
                    GeneraToken(linea, "Error");
                break;

                case 10:
                    GeneraToken(linea.Substring(0,pos), "Recervada");
                    AnalizarLinea(linea.Substring(pos + 1), 0, 0);
                break;
            }
        }

        private void GeneraToken(string linea, string v)
        {
            TablaDeSimbolos.Add(new Simbolo()
            {
                Lexema = linea,
                Token = v
            });
        }
    }
}
